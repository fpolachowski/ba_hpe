import os
import json
import numpy as np

import torch
from torch.utils.data import Dataset
from PIL import Image

from core.config import config as cfg


class MPIIDataset(Dataset):
    def __init__(self, transform=None):
        self.config = cfg['dataset']['MPII']
        self.root_dir = self.config['root']
        self.transform = transform

        self.data, self.targets = self.load_data()

    def load_data(self):
        """
        Returns:
            tuple: (data, targets) where data are all images and targets are the corresponding joint locations.
        """
        data = []
        targets = []

        with open(os.path.join(self.root_dir, 'annotations.json')) as f:
            annotations = json.load(f)

            for anno in annotations:
                data.append(anno['name'])

                joints = anno['joints']
                labels = []

                for i in range(len(joints)):
                    for joint in joints:
                        if joint['id'] == i:
                            pos = joint['pos']
                            labels.append([pos[1], pos[0]])
                            break
                targets.extend([labels])

        return data, targets

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, targets) where targets are the joint locations.
        """
        if torch.is_tensor(index):
            index = index.tolist()

        image_name = os.path.join(self.root_dir, 'images', self.data[index])
        image = Image.open(image_name)
        targets = np.copy(self.targets[index])

        if self.transform is not None:
            sample = (image, targets)
            image, targets = self.transform(sample)

        return image, targets
