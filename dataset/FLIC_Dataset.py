import os
import pandas as pd
import numpy as np

import torch
from torch.utils.data import Dataset
from PIL import Image

from core.config import config as cfg


class FLICDataset(Dataset):
    '''Frames Labled in Cinema dataset.'''

    def __init__(self, transform=None):
        """
        Args:
            transform (callable, optional): Optional transform to be applied to each sample.
        """

        self.config = cfg['dataset']
        self.root_dir = self.config['FLIC']['root']
        self.transform = transform

        self.data, self.targets = self.load_data()

    def load_data(self):
        """
        Returns:
            tuple: (data, targets) where data are all images and targets are the corresponding joint locations.
        """
        data = []
        targets = []

        # annotations format name,(x,y)*11,isTrain
        # annotation order rsho, relb, rwri, lsho, lelb, lwri, rhip, lhip, reye, leye, nose
        annotations = pd.read_csv(os.path.join(
            self.root_dir, 'annotations.csv'))
        for i in range(len(annotations)):
            img_name = annotations.iloc[i, 0]

            labels = annotations.iloc[i, 1:23]
            labels = np.asarray(labels)
            labels = labels.astype('float').reshape(-1, 2)

            # rearange from w,h to h,w
            labels[:, [0, 1]] = labels[:, [1, 0]]

            data.append(img_name)
            targets.extend([labels])

        return data, targets

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            tuple: (image, targets) where targets are the joint locations.
        """
        if torch.is_tensor(index):
            index = index.tolist()

        image_name = os.path.join(self.root_dir, 'images', self.data[index])
        image = Image.open(image_name)
        targets = np.copy(self.targets[index])

        if self.transform is not None:
            sample = (image, targets)
            image, targets = self.transform(sample)

        return image, targets
