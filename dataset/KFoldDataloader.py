import torch
import random

""" Custom Dataloader
    Takes dataset in k folds and splits each fold into chunks of a specific batch_size
"""


class DataLoader():
    def __init__(self, dataset, sets, batch_size=1, shuffle=False):
        self.dataset = dataset
        self.sets = sets
        self.batch_size = batch_size
        if shuffle:
            self.shuffle()
        else:
            self.indizies = list(self.divide_chunks(
                self.sets, self.batch_size))

    def shuffle(self):
        random.shuffle(self.sets)
        self.indizies = list(self.divide_chunks(self.sets, self.batch_size))

    def divide_chunks(self, ar, size):
        for i in range(0, len(ar), size):
            yield ar[i:i+size]

    def __len__(self):
        return len(self.indizies)

    def __getitem__(self, index):
        splits = self.indizies[index]
        imgs = []
        targets = []
        for split in splits:
            img, target = self.dataset[split]
            imgs.append(img)
            targets.append(target)
        targets = torch.stack(targets)
        images = torch.stack(imgs)
        return (images, targets)
