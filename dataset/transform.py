import torch
from PIL import Image
import numpy as np
from skimage.transform import resize

import torchvision.transforms as transforms

from core.config import config


class Rescale(object):
    """Rescale the image to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, the smaller image edge is matched
            to output_size, keeping aspect ratio the same.
    """

    def __init__(self, size=None):
        self.size = size

    def __call__(self, sample):
        image, target = sample

        w, h = image.size
        if self.size is None:
            output_size = config['dataset']['image_size']
        else:
            output_size = self.size
        if isinstance(output_size, int):
            if h > w:
                new_h, new_w = output_size * h / w, output_size
            else:
                new_h, new_w = output_size, output_size * w / h
        else:
            new_h, new_w = output_size

        new_h, new_w = int(new_h), int(new_w)

        image = image.resize((new_h, new_w))
        for i in range(len(target)):
            target[i] = [(target[i][0]/h) * new_h, (target[i][1]/w) * new_w]

        return (image, target)


class ToTensor(object):
    """Convert ndarrays to Tensors."""

    def __call__(self, sample):
        image, targets = sample
        transf = transforms.ToTensor()
        return (transf(image), torch.tensor(targets, dtype=torch.float32))


class RescaleTargets(object):
    def __init__(self):
        image_size = config['dataset']['image_size']
        target_size = config['network']['heatmap_size']
        self.ratio = torch.tensor(
            [x / y for x, y in zip(image_size, target_size)], dtype=torch.float32)

    def __call__(self, sample):
        image, targets = sample
        for idx in range(len(targets)):
            targets[idx] /= self.ratio

        return image, targets


class Normalize(object):
    """Convert ndarrays to Tensors."""

    def __call__(self, sample):
        image, targets = sample
        mean = config['dataset']['current']['mean']
        std = config['dataset']['current']['std']
        transf = transforms.Normalize(mean=mean, std=std)
        return (transf(image), targets)


class Crop(object):
    """Corps an image depending on the center of the body"""

    def __call__(self, sample):
        image, targets = sample

        # Center point given by left hip and right sholder
        # Note joint coords are in h,w
        lhip = targets[config['dataset']['current']['torsobox_joints'][0]]
        rsho = targets[config['dataset']['current']['torsobox_joints'][1]]
        center = (lhip + rsho)/2

        # maximum x and y offset given by farthest joints
        max_dist_x, max_dist_y = 0, 0
        for joint in targets:
            diff = joint - center
            max_dist_y = abs(diff[0]) if abs(
                diff[0]) > max_dist_y else max_dist_y
            max_dist_x = abs(diff[1]) if abs(
                diff[1]) > max_dist_x else max_dist_x

        # Boundingbox
        offset = config['dataset']['crop_offset']
        b_w = 2 * (offset + max_dist_x)
        b_h = 2 * (offset + max_dist_y)

        x_m = int(center[1] - b_w/2) if int(center[1] - b_w/2) > 0 else 0
        x_p = int(center[1] + b_w/2) if int(center[1] +
                                            b_w/2) < image.size[0] else image.size[0]

        if config['dataset']['current']['name'] == 'MPII':
            y_m = int(center[0] - b_h/2) if int(center[0] - b_h/2) > 0 else 0
            y_p = int(center[0] + b_h/2) if int(center[0] +
                                                b_h/2) < image.size[1] else image.size[1]
        else:
            y_m = int(center[0] - b_h/2) if int(center[0] - b_h/2) > 55 else 0
            y_p = int(center[0] + b_h/2) if int(center[0] +
                                                b_h/2) < image.size[1] - 61 else image.size[1] - 61

        # Crop image
        image = image.crop((x_m, y_m, x_p, y_p))

        corner = [y_m, x_m]

        # Change Joint coordinates relative to the box corner
        new_targets = [x - corner for x in targets]

        return (image, new_targets)
