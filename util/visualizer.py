from core.config import config
import numpy as np
import matplotlib
if not config['debug']:
    matplotlib.use('Agg')
from matplotlib import pyplot as plt
"""
    Data visualization
"""

body_parts = config['dataset']['current']['body_vis']


def save(images, predictions, labels, epoch, run):
    num = 0
    image_size = config['dataset']['image_size']
    target_size = config['network']['heatmap_size']
    ratio = [x / y for x, y in zip(image_size, target_size)]
    for img, pred, label in zip(images, predictions, labels):
        plt.title('Epoch {} Image {}'.format(epoch, num))
        img = img.cpu().numpy().transpose((1, 2, 0))
        plt.imshow(np.clip(img, 0, 1))
        pred = pred.cpu().detach().numpy() * ratio[0]
        label = label.cpu().numpy() * ratio[0]
        for start, end in body_parts:
            joint_pred_start = pred[start]
            joint_pred_end = pred[end]

            joint_label_start = label[start]
            joint_label_end = label[end]

            x_pred = [joint_pred_start[1], joint_pred_end[1]]
            y_pred = [joint_pred_start[0], joint_pred_end[0]]

            x_label = [joint_label_start[1], joint_label_end[1]]
            y_label = [joint_label_start[0], joint_label_end[0]]

            plt.plot(x_pred, y_pred, 'r', linewidth=1)
            plt.plot(x_label, y_label, 'g', linewidth=1)
        plt.savefig(
            '{}/{}_epoch_{}_image_{}.png'.format(config['image_folder'], epoch, num, run))
        plt.close()
        num += 1
        if num == config['num_images']:
            break


def showImage_AnnCoords(images, predictions, targets):
    fig = plt.figure()
    columns = images.shape[0] % 3
    rows = 2 if images.shape[0] > 3 else 1
    for i in range(1, columns*rows + 1):
        img = images[i-1].cpu().numpy().transpose((1, 2, 0))
        fig.add_subplot(rows, columns, i)
        plt.imshow(np.clip(img, 0, 1))
        preds = predictions[i-1].cpu().detach().numpy()
        labels = targets[i-1].cpu().numpy()

        for start, end in body_parts:
            joint_pred_start = preds[start]
            joint_pred_end = preds[end]

            joint_label_start = labels[start]
            joint_label_end = labels[end]

            x_pred = [joint_pred_start[1], joint_pred_end[1]]
            y_pred = [joint_pred_start[0], joint_pred_end[0]]

            x_label = [joint_label_start[1], joint_label_end[1]]
            y_label = [joint_label_start[0], joint_label_end[0]]

            plt.plot(x_pred, y_pred, 'r')
            plt.plot(x_label, y_label, 'g')

    plt.show()


def showImage(images):
    fig = plt.figure()
    columns = images.shape[0] % 3
    rows = 2 if images.shape[0] > 3 else 1
    for i in range(1, columns*rows + 1):
        img = images[i-1].cpu().numpy().transpose((1, 2, 0))
        fig.add_subplot(rows, columns, i)
        plt.imshow(img)

    plt.show()


def display(dataset):
    for img, annotation in dataset:
        img = img.cpu().numpy().transpose((1, 2, 0))
        plt.imshow(img)
        annotation = annotation.cpu().numpy() * 4

        for start, end in config['dataset']['current']['body_vis']:
            start = annotation[start]
            end = annotation[end]

            x = [start[1], end[1]]
            y = [start[0], end[0]]

            plt.plot(x, y, 'g', linewidth=1)

        plt.show()
