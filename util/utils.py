import torch
import random
import numpy as np
import os
from torch.optim import Adam, SGD
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from torchvision import transforms
from core.sampler import SequentialSampler

from models.ResDeconv import ResDeconv
from dataset.transform import Rescale, ToTensor, Normalize, RescaleTargets, Crop
from dataset.FLIC_Dataset import FLICDataset
from dataset.MPII_Dataset import MPIIDataset
from core.config import config


def seed_everything(seed=None):
    if seed is None:
        seed = random.randint(1, 10000)

    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False


def createSplit(dataset, batch_size, shuffle=False, split=0.1):
    dataset_size = len(dataset)
    indices = list(range(dataset_size))
    split = int(np.floor(split * dataset_size))
    if shuffle:
        np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]

    # Creating PT data samplers and loaders:
    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SequentialSampler(val_indices)

    train_loader = DataLoader(dataset, batch_size=batch_size,
                              sampler=train_sampler)
    validation_loader = DataLoader(dataset, batch_size=1,
                                   sampler=valid_sampler)

    return train_loader, validation_loader


def createLoader(dataset, batch_size, train_set, val_set):
    if config['train']['shuffle']:
        np.random.shuffle(train_set)

    train_sampler = SubsetRandomSampler(train_set)
    valid_sampler = SequentialSampler(val_set)
    train_loader = DataLoader(dataset, batch_size=batch_size,
                              sampler=train_sampler)
    validation_loader = DataLoader(
        dataset, batch_size=batch_size, sampler=valid_sampler)

    return train_loader, validation_loader


def get_dataset():
    transform = [Crop(), Rescale(), ToTensor(), Normalize()]
    if config['dataset']['current']['name'] == 'FLIC':
        return FLICDataset(transform=transforms.Compose(transform))
    elif config['dataset']['current']['name'] == 'MPII':
        return MPIIDataset(transform=transforms.Compose(transform))


def get_optimizer(run, model):
    optimizer = None
    if config['train']['optim'] == 'sgd':
        optimizer = SGD(
            model.parameters(),
            lr=run.lr,
            momentum=run.momentum,
            weight_decay=run.weight_decay
        )
    elif config['train']['optim'] == 'adam':
        optimizer = Adam(
            model.parameters(),
            lr=run.lr,
            weight_decay=run.weight_decay
        )

    return optimizer


def getMeanDist(train_loader, eval_loader):
    mdist = 0.
    dist_indv = [0.] * len(config['dataset']['current']['body'])
    for _, labels in train_loader:
        for label in labels:
            for start_joint, end_joint in config['dataset']['current']['body']:
                mdist += torch.dist(
                    label[start_joint], label[end_joint]).item() / len(labels) / len(config['dataset']['current']['body'])
                dist_indv[start_joint] += torch.dist(
                    label[start_joint], label[end_joint]).item() / len(labels)

    for _, labels in eval_loader:
        for label in labels:
            for start_joint, end_joint in config['dataset']['current']['body']:
                mdist += torch.dist(
                    label[start_joint], label[end_joint]).item() / len(labels) / len(config['dataset']['current']['body'])
                dist_indv[start_joint] += torch.dist(
                    label[start_joint], label[end_joint]).item() / len(labels)

    print([x / (len(train_loader) + len(eval_loader)) for x in dist_indv])
    return mdist / (len(train_loader) + len(eval_loader))


def calcNorm(dataset):
    loader = DataLoader(dataset, batch_size=10)

    mean = 0.
    std = 0.
    nb_samples = 0.
    for data in loader:
        data = data[0]
        batch_samples = data.size(0)
        data = data.view(batch_samples, data.size(1), -1)
        mean += data.mean(2).sum(0)
        std += data.std(2).sum(0)
        nb_samples += batch_samples

    mean /= nb_samples
    std /= nb_samples

    print(mean)
    print(std)
