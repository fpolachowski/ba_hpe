import torch

from core.config import config

"""Metrics for human pose estimation"""

body = config['dataset']['current']['body']


def dist(predictions, labels):
    distances = [0.] * predictions.size(1)
    for pred, label in zip(predictions, labels):
        for idx in range(pred.size(0)):
            distance_pred = torch.dist(pred[idx], label[idx])
            distances[idx] += distance_pred.item()
    return [x / predictions.size(0) for x in distances]


def pcp(predictions, labels, alpha=0.1):
    num_correct = [0.] * predictions.size(1)
    for pred, label in zip(predictions, labels):
        for start_joint, end_joint in body:
            distance_part = torch.dist(
                label[start_joint], label[end_joint])
            distance_pred = torch.dist(
                pred[start_joint], label[start_joint])
            if(distance_pred <= distance_part * alpha):
                num_correct[start_joint] += 1
    return [x / predictions.size(0) for x in num_correct]


def pcpm(predictions, labels, mean_length, alpha=0.5):
    num_correct = [0.] * predictions.size(1)
    for pred, label in zip(predictions, labels):
        for start_joint, _ in body:
            distance_pred = torch.dist(
                pred[start_joint], label[start_joint])
            if(distance_pred <= mean_length * alpha):
                num_correct[start_joint] += 1
    return [x / predictions.size(0) for x in num_correct]


def pckb(predictions, labels, torso=(7, 0), alpha=0.5):
    num_correct = [0.] * predictions.size(1)
    for pred, label in zip(predictions, labels):
        for start_joint, _ in body:
            distance_gt = torch.dist(label[torso[0]], label[torso[1]])
            distance_pred = torch.dist(
                pred[start_joint], label[start_joint])
            if(distance_pred <= distance_gt * alpha):
                num_correct[start_joint] += 1
    return [x / predictions.size(0) for x in num_correct]


def pckh(predictions, labels, head=(8, 9), alpha=0.5):
    num_correct = [0.] * predictions.size(1)
    for pred, label in zip(predictions, labels):
        distance = torch.dist(
            label[head[0]], label[head[1]])
        for start_joint, _ in body:
            distance_pred = torch.dist(
                pred[start_joint], label[start_joint])
            if(distance_pred <= distance * alpha):
                num_correct[start_joint] += 1
    return [x / predictions.size(0) for x in num_correct]
