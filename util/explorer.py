import torch
import json
from core.config import config
from util.visualizer import save
from util.metrics import pcp, dist

from time import time
from csv import writer


class Logger():
    def __init__(self, run, i):
        self.run = run
        self.fileName = 'kfold_{}_{}'.format(i, self.run)

        self.iteration = 0
        self.kfold = 0
        self.reset()

    def reset(self):
        self.loss = 0.
        self.acc = [0.] * config['dataset']['current']['num_joints']
        self.time = time()

    def next_Kfold(self):
        self.kfold += 1
        self.iteration = 0
        self.eval_acc = [0.] * config['dataset']['current']['num_joints']
        self.eval_loss = 0.
        self.eval_dist = [0.] * config['dataset']['current']['num_joints']
        self.reset()

    def _log(self, data, _type='train'):
        fileName = '{}/{}_{}_{}.csv'.format(
            config['doc_folder'], self.kfold, self.fileName, _type)
        with open(fileName, mode='a', newline='') as f:
            w = writer(f)
            w.writerow(data)

    def step(self, epoch, loss, predictions, labels):
        self.iteration += 1

        self.loss += loss.item() / len(predictions)
        self.acc = [sum(x) for x in zip(
            self.acc, pcp(predictions, labels, alpha=config['pcp_acc']))]

        ## Store data of every x-th batch iteration ##
        steps = config['doc_steps']
        if self.iteration % steps == 0:
            loss = self.loss / steps
            acc = [round(x * 100 / steps, 2) for x in self.acc]
            duration = time() - self.time

            data = [self.iteration, loss, acc, duration]
            self._log(data, _type='train')

            self.reset()

    def saveImage(self, images, predictions, labels, epoch):
        save(images, predictions, labels, epoch, self.fileName)

    def endfold(self, network, test_length):
        self._log('#', _type='train')
        self._log('#', _type='eval')

    def evaluate(self, predictions, labels, loss):
        metric = [x for x in pcp(predictions,
                                 labels, alpha=config['pcp_acc'])]
        distance = [x for x in dist(predictions, labels)]
        self.eval_dist = [sum(x) for x in zip(self.eval_dist, distance)]
        self.eval_acc = [sum(x) for x in zip(
            self.eval_acc, metric)]
        self.eval_loss += loss.item() / len(labels)

    def endEval(self, test_length):
        dist = [x / test_length for x in self.eval_dist]
        acc = [x / test_length for x in self.eval_acc]
        eval_loss = self.eval_loss / test_length

        self.eval_acc = [0.] * config['dataset']['current']['num_joints']
        self.eval_loss = 0.
        self.eval_dist = [0.] * config['dataset']['current']['num_joints']

        self._log([dist, acc, eval_loss], _type='eval')

    def storeNetwork(self, network):
        fileName = '{}/{}_{}.pth'.format(
            config['model_folder'], self.kfold, self.fileName)
        torch.save(network.state_dict(), fileName)
