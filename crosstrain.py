import torch
import numpy
from glob import glob
from core.config import config
from core.functions import evaluate, train_epoch
from models.ResDeconv import ResDeconv
from models.ResFCN import ResFCN
from models.FCN8s_v2 import FCN8s_v2
from models.spatial_softargmax import SoftArgMax
from core.loss import ElasticNetLossPoses, DistanceLossPoses
from util.builder import Builder
from util.explorer import Logger
from util.utils import get_optimizer, get_dataset, seed_everything, createSplit


dataset = get_dataset()
files = glob('store/*.pth')

for path in files:
    for run in Builder.create(config['parameters']):
        for i in range(config['train']['num_runs']):
            seed_everything()
            log = Logger(run, i)

            num_joints = None
            if config['cross']:
                if config['dataset']['current']['name'] == 'FLIC':
                    num_joints = 16
                else:
                    num_joints = 11

            network = ResFCN(num_joints=num_joints)
            network.load_state_dict(torch.load(
                path, map_location=config['device']))

            network.conv1.weight.requires_grad = False
            network.bn1.weight.requires_grad = False
            network.bn1.bias.requires_grad = False

            for param in network.block1.parameters():
                param.requires_grad = False

            for param in network.block2.parameters():
                param.requires_grad = False

            for param in network.block3.parameters():
                param.requires_grad = False

            for param in network.block4.parameters():
                param.requires_grad = False

            # for name, param in network.named_parameters():
            #     print(name, param.requires_grad)

            network.fcn = FCN8s_v2()
            network.softargmax = SoftArgMax()
            network.to(config['device'])
            optimizer = get_optimizer(run, network)
            criterion = DistanceLossPoses()

            log.next_Kfold()

            train_loader, eval_loader = createSplit(
                dataset, run.batch_size, shuffle=True, split=0.1)

            for epoch in range(run.end_epoch):
                train_epoch(train_loader, network, criterion,
                            optimizer, epoch, log)

                evaluate(eval_loader, network, criterion, log, epoch)

            log.endfold(network, len(eval_loader))

            if config['store_results']:
                log.storeNetwork(network)
