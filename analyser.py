import csv
import os
from glob import glob
import pandas as pd
import matplotlib
from matplotlib import pyplot as plt
import numpy
import json
from core.config import config


def samePlot(fold, smooth=True, grids=True):
    fig = plt.figure()
    ax = fig.add_subplot(121, label="Loss Test")
    ax2 = fig.add_subplot(121, label="Loss Train", frame_on=False)

    ax3 = fig.add_subplot(122, label="Acc Test")
    ax4 = fig.add_subplot(122, label="Acc Train", frame_on=False)
    fig.suptitle(fold['params'])
    fig.set_size_inches(20, 10)

    ########## Training Part #############

    train = fold['train']

    y = pd.Series(train['y'])
    if smooth:
        y = y[y.between(y.quantile(.15), y.quantile(.80))]
    x = train['x'][: len(y)]

    ax2.set(xlabel='Iterations', ylabel='Loss')
    ax2.xaxis.tick_top()
    ax2.yaxis.tick_right()
    ax2.xaxis.set_label_position('top')
    ax2.yaxis.set_label_position('right')

    if smooth:
        line2, = ax2.plot(x, _smooth(y, window_len=50)[
            :len(x)], '-', label='Iterations' + '-Loss')
    else:
        line2, = ax2.plot(x, y, '-', label='Iterations' + '-Loss')

    acc = []
    for li in train['acc']:
        av = sum(li) / len(li)
        acc.append(av)

    y = pd.Series(acc)

    ax4.set(xlabel='Iterations', ylabel='Acc %')
    ax4.xaxis.tick_top()
    ax4.yaxis.tick_right()
    ax4.xaxis.set_label_position('top')
    ax4.yaxis.set_label_position('right')
    if smooth:
        line4, = ax4.plot(x, _smooth(y, window_len=50)[
            :len(x)], '-', label='Iterations' + '-Acc')
    else:
        line4, = ax4.plot(x, y, '-', label='Iterations' + '-Acc')

    ########## Evaluation Part #############

    test = fold['test']

    y = pd.Series(test['y'])
    x = test['x'][: len(y)]

    ax.set_xlabel('Epoch', color='C1')
    ax.set_ylabel('Loss', color='C1')
    ax.tick_params(axis='x', colors='C1')
    ax.tick_params(axis='y', colors='C1')

    line, = ax.plot(x, y, '-', label='Epoch' + '-Loss', color='C1')

    ax.legend((line2, line), ('Train', 'Test'))
    acc = []
    for li in test['acc']:
        av = sum(li) / len(li)
        acc.append(av)

    y = pd.Series(acc)

    ax3.set_xlabel('Epoch', color='C1')
    ax3.set_ylabel('Loss', color='C1')
    ax3.tick_params(axis='x', colors='C1')
    ax3.tick_params(axis='y', colors='C1')

    line3, = ax3.plot(x, y, '-', label='Epoch' + '-Acc', color='C1')

    ax3.legend((line4, line3), ('Train', 'Test'))
    if grids:
        num_ticks = int((len(x) + 1) / 30)
        ticks = numpy.arange(1, len(x) + num_ticks, num_ticks)
        ax.set_xticks(ticks)
        ax.grid()
        ax3.set_xticks(ticks)
        ax3.grid()

    plt.show()


def getName(fileName):
    values = {}
    params = fileName.split('(')[-1].split(')')[0].split(',')
    values = {p.split('=')[0]: p.split('=')[1] for p in params}
    values['kfold'] = fileName.split('\\')[-1].split('_')[0]
    values['iteration'] = fileName.split('\\')[-1].split('_')[2]
    return values


def stringToList(string):
    string = string.replace('[', '').replace(']', '')
    li = string.split(',')
    ret = [float(x) for x in li]
    return ret


def _smooth(x, window_len=11,):
    s = numpy.r_[x[window_len-1:0:-1], x, x[-2:-window_len-1:-1]]
    w = eval('numpy.hanning(window_len)')
    y = numpy.convolve(w/w.sum(), s, mode='valid')
    return y


def combine(run_col, eval_col):
    folds = []
    for run in run_col:
        for ev in eval_col:
            if run['params'] == ev['params']:
                fold = {
                    'params': run['params'],
                    'train': run,
                    'test': ev
                }
                folds.append(fold)
                break
    return folds


def average(folds):
    fold = {
        'params': folds[0]['params'],
        'train': {},
        'test': {}
    }
    xs = [0] * len(folds[0]['train']['x'])
    ys = [0] * len(folds[0]['train']['y'])
    times = [0] * len(folds[0]['train']['time'])
    accs = [[0.] * len(folds[0]['train']['acc'][0])] * \
        len(folds[0]['train']['acc'])
    for f in folds:
        train = f['train']
        xs = numpy.add(xs, train['x'])
        ys = numpy.add(ys, train['y'])
        times = numpy.add(times, train['time'])
        for index, acc in enumerate(train['acc']):
            accs[index] = numpy.add(accs[index], acc)

    # fit iterations to epochs
    xs = [150 * x / len(folds) / 33900 for x in xs]
    ys = [y / len(folds) for y in ys]
    times = [time / len(folds) for time in times]
    accs = [[ac / len(folds) for ac in acc] for acc in accs]

    fold['train'] = {
        'x': xs,
        'y': ys,
        'acc': accs,
        'time': times
    }

    xs = [0] * len(folds[0]['test']['x'])
    ys = [0] * len(folds[0]['test']['y'])
    distances = [[0.] * len(folds[0]['test']['distance'][0])
                 ] * len(folds[0]['test']['distance'])
    accs = [[0.] * len(folds[0]['test']['acc'][0])] * \
        len(folds[0]['test']['acc'])
    for f in folds:
        ev = f['test']
        xs = numpy.add(xs, ev['x'])
        ys = numpy.add(ys, ev['y'])
        for index, dist in enumerate(ev['distance']):
            distances[index] = numpy.add(distances[index], dist)
        for index, acc in enumerate(ev['acc']):
            accs[index] = numpy.add(accs[index], acc)

    xs = [x / len(folds) for x in xs]
    ys = [y / len(folds) for y in ys]
    distances = [[dist / len(folds) for dist in distance]
                 for distance in distances]
    accs = [[ac / len(folds) for ac in acc] for acc in accs]

    fold['test'] = {
        'x': xs,
        'y': ys,
        'acc': accs,
        'distance': distances
    }

    return fold


def createRunCollection(files):
    collection = []
    for run_file in files:
        with open(run_file) as f:
            data = {
                'params': getName(run_file),
                'x': [],
                'y': [],
                'acc': [],
                'time': []
            }
            date = csv.reader(f)
            for row in date:
                if row[0] != '#':
                    data['x'].append(int(row[0]))
                    data['y'].append(float(row[1]))
                    data['acc'].append(stringToList(row[2]))
                    data['time'].append(float(row[3]))
                # else:
                #     collection.append(data)
                #     data = {
                #         'params': getName(run_file),
                #         'x': [],
                #         'y': [],
                #         'acc': [],
                #         'time': []
                #     }
            collection.append(data)

    return collection


def evaluate(files):
    runs = []
    for run_file in files:

        with open(run_file) as f:
            run = {
                'params': getName(run_file),
                'acc': [],
                'distance': [],
                'x': [],
                'y': [],
            }
            date = csv.reader(f)
            amount = 0

            for row in date:
                if row[0] != '#':
                    amount += 1
                    run['x'].append(amount)
                    run['y'].append(float(row[2]))
                    run['acc'].append([round(x * 100, 2)
                                       for x in stringToList(row[1])])
                    run['distance'].append(stringToList(row[0]))
            runs.append(run)

    return runs


def evaluateRun(fold, smooth=True, grids=True):
    fontsize = 16
    fig, axs = plt.subplots(2, 2)
    fig.set_size_inches(20, 10)

    plt.rcParams.update({'font.size': 16})

    ########## Training Part #############

    train = fold['train']

    y = pd.Series(train['y'])
    if smooth:
        y = y[y.between(y.quantile(.15), y.quantile(.80))]
    x = train['x'][: len(y)]

    # axs[0, 0].set(xlabel='Epoche', ylabel='Loss')
    axs[0, 0].legend()
    if smooth:
        line2, = axs[0, 0].plot(x, _smooth(y, window_len=50)[
            :len(x)], '-', label='Epoch' + '-Loss', color='red')
    else:
        line2, = axs[0, 0].plot(
            x, y, '-', label='Epoch' + '-Loss', color='red')

    axs[0, 0].legend([line2], ['Loss Training'])

    acc = []
    for li in train['acc']:
        av = sum(li) / len(li)
        acc.append(av)

    y = pd.Series(acc)

    # axs[0, 1].set(xlabel='Epoche', ylabel='Genauigkeit [%]')
    if smooth:
        line4, = axs[0, 1].plot(x, _smooth(y, window_len=50)[
            :len(x)], '-', label='Epoch' + '-Acc', color='green')
    else:
        line4, = axs[0, 1].plot(
            x, y, '-', label='Epoch' + '-Acc', color='green')

    axs[0, 1].legend([line4], ['Genauigkeit Training'], loc='lower right')

    if grids:
        ticks = numpy.arange(0, x[-1] + 1, 10)
        axs[0, 0].set_xticks(ticks)
        axs[0, 1].set_xticks(ticks)
        axs[0, 1].set_xlabel('Epoche', fontsize=fontsize)
        axs[0, 1].set_ylabel('Genauigkeit [%]', fontsize=fontsize)
        axs[0, 0].set_xlabel('Epoche', fontsize=fontsize)
        axs[0, 0].set_ylabel('Loss', fontsize=fontsize)

    ########## Evaluation Part #############

    test = fold['test']

    y = pd.Series(test['y'])
    x = test['x'][: len(y)]

    # axs[1, 0].set(xlabel='Epoche', ylabel='Loss')
    line, = axs[1, 0].plot(x, y, '-', label='Epoch' + '-Loss', color='red')

    axs[1, 0].legend([line], ['Loss Validierung'])

    acc = []
    for li in test['acc']:
        av = sum(li) / len(li)
        acc.append(av)

    y = pd.Series(acc)

    # axs[1, 1].set(xlabel='Epoche', ylabel='Genauigkeit [%]')
    line3, = axs[1, 1].plot(x, y, '-', label='Epoch' + '-Acc', color='green')

    axs[1, 1].legend([line3], ['Genauigkeit Validierung'], loc='lower right')

    if grids:
        ticks = numpy.arange(0, len(x) + 1, 10)
        axs[1, 0].set_xticks(ticks)
        axs[1, 1].set_xticks(ticks)

        axs[1, 1].set_xlabel('Epoche', fontsize=fontsize)
        axs[1, 1].set_ylabel('Genauigkeit [%]', fontsize=fontsize)

        axs[1, 0].set_xlabel('Epoche', fontsize=fontsize)
        axs[1, 0].set_ylabel('Loss', fontsize=fontsize)

    plt.show()


def plot_distance(collections):
    fig = plt.figure()
    ax0 = fig.add_subplot(121, label="Distance run0")

    biggest = 0.
    x = collections[0]['test']['x']
    for index, collection in enumerate(collections):
        ev = collection['test']
        dist = []
        for li in ev['distance']:
            ad = sum(li) / len(li)
            biggest = ad if ad > biggest else biggest
            dist.append(ad)

        y = pd.Series(dist)

        if index == 0:
            ax0.set(xlabel='Epoch', ylabel='Distance')
            line0, = ax0.plot(x, y, '-', label='Epoch-Dist', color='blue')
        elif index == 1:
            line1, = ax0.plot(x, y, '-', label='Epoch-Dist', color='green')
        elif index == 2:
            line2, = ax0.plot(x, y, '-', label='Epoch-Dist', color='red')
        elif index == 3:
            line3, = ax0.plot(x, y, '-', label='Epoch-Dist', color='cyan')
        elif index == 4:
            line4, = ax0.plot(x, y, '-', label='Epoch-Dist', color='yellow')

    ticks = numpy.arange(0, biggest + 1, 3)
    ax0.set_yticks(ticks)
    ticks = numpy.arange(0, 51, 5)
    ax0.set_xticks(ticks)

    ax0.legend((line0, line1, line2, line3, line4),
               ('Baseline', 'F1', 'F2', 'F3', 'F4'), loc='upper right')

    plt.show()


def plot_time(run_col):
    fig = plt.figure()
    ax = fig.add_subplot(111, label="Dist Test")
    x = run_col['x']
    time = run_col['time']

    ov_time = sum(time)
    h = int(ov_time / 3600)
    m = int((ov_time / 60) - (h * 60))
    s = int(ov_time % 60)

    print('This experiment took ', str(h) +
          ':' + str(m) + ':' + str(s))

    y = pd.Series(time)

    ax.set(xlabel='Iterations', ylabel='Time')
    ax.plot(x, y, '-', label='Epoch-Dist')

    plt.show()


def join_joints(data):
    res = {}
    alphas = []
    for alpha in data:
        alphas.append(round(float(alpha), 4))
        results = data[alpha]
        for key in results:
            if key not in res:
                res[key] = []
            res[key].append(results[key])
    return alphas, res


def run():
    run_files = glob(os.path.join('runs', '*_kfold_*_train.csv'))
    eval_files = glob(os.path.join('runs', '*_kfold_*_eval.csv'))

    run_col = createRunCollection(run_files)
    eval_col = evaluate(eval_files)

    folds = combine(run_col, eval_col)

    folds = [average(folds)]

    # plot_distance(folds)

    for fold in folds:
        # samePlot(fold, smooth=False, grids=True)
        evaluateRun(fold, smooth=False, grids=True)
        # plot_time(fold['train'])


def tests():
    tests = glob(os.path.join('runs', '*.json'))
    for test in tests:
        with open(test) as f:
            data = json.load(f)
            x, data = join_joints(data)

            if config['dataset']['current']['name'] == 'FLIC':
                data = {
                    'Kopf': numpy.add(numpy.add(data['right shoulder'], data['left shoulder']), data['nose']) / 0.03,
                    'Schultern': numpy.add(data['right shoulder'], data['left shoulder']) / 0.02,
                    'Ellbogen': numpy.add(data['right elbow'], data['left elbow']) / 0.02,
                    'Handgelenke': numpy.add(data['right wrist'], data['left wrist']) / 0.02,
                    'Hüfte': numpy.add(data['right hip'], data['left hip']) / 0.02
                }
            else:
                data = {
                    'Kopf': numpy.add(data['upper neck'], data['head top']) / 0.02,
                    'Schultern': numpy.add(numpy.add(data['right shoulder'], data['left shoulder']), data['thorax']) / 0.03,
                    'Ellbogen': numpy.add(data['right elbow'], data['left elbow']) / 0.02,
                    'Handgelenke': numpy.add(data['right wrist'], data['left wrist']) / 0.02,
                    'Hüfte': numpy.add(numpy.add(data['right hip'], data['left hip']), data['pelvis']) / 0.03,
                    'Knie': numpy.add(data['right knee'], data['left knee']) / 0.02,
                    'Knöchel': numpy.add(data['right ankle'], data['left ankle']) / 0.02,
                }

            i = 0
            ticks = numpy.arange(0., 124, 25)
            plt.xlabel('alpha')
            plt.ylabel('Accuracy [%]')
            plt.yticks(ticks)
            plt.grid()
            legend = {}
            for key in data:
                line, = plt.plot(
                    x, data[key], linewidth=3 if key == 'Hüfte' else 1)
                legend[key] = line

            plt.legend((legend.values()), list(
                legend.keys()), loc='lower right')

        plt.show()


# run()
tests()
