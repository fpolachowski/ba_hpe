import torch.nn as nn
from core.config import config


class FCN8s_v2(nn.Module):

    def __init__(self, num_joints=None):
        super().__init__()
        if num_joints is None:
            self.n_class = config['dataset']['current']['num_joints']
        else:
            self.n_class = num_joints
        self.classifier = nn.Conv2d(512, self.n_class, kernel_size=1)
        self.score_pool3 = nn.Conv2d(256, self.n_class, kernel_size=1)
        self.score_pool4 = nn.Conv2d(512, self.n_class, kernel_size=1)

        self.relu = nn.ReLU(inplace=True)
        self.deconv1 = nn.ConvTranspose2d(
            self.n_class, self.n_class, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.bn1 = nn.BatchNorm2d(self.n_class)
        self.deconv2 = nn.ConvTranspose2d(
            self.n_class, self.n_class, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.bn2 = nn.BatchNorm2d(self.n_class)
        self.deconv3 = nn.ConvTranspose2d(
            self.n_class, self.n_class, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.bn3 = nn.BatchNorm2d(self.n_class)
        self.deconv4 = nn.ConvTranspose2d(
            self.n_class, self.n_class, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.bn4 = nn.BatchNorm2d(self.n_class)
        self.deconv5 = nn.ConvTranspose2d(
            self.n_class, self.n_class, kernel_size=3, stride=2, padding=1, dilation=1, output_padding=1)
        self.bn5 = nn.BatchNorm2d(self.n_class)

    def forward(self, output):
        x5 = output['x5']  # size=(N, 512, x.H/32, x.W/32)
        x4 = output['x4']  # size=(N, 512, x.H/16, x.W/16)
        x3 = output['x3']  # size=(N, 256, x.H/8,  x.W/8)

        # size=(N, num_classes, x.H/32, x.W/32)
        score = self.classifier(x5)

        # size=(N, num_classes, x.H/16, x.W/16)
        score = self.relu(self.deconv1(score))

        x4 = self.score_pool4(x4)
        # element-wise add, size=(N, 512, x.H/16, x.W/16)
        score = self.bn1(score + x4)
        # size=(N, num_classes, x.H/8, x.W/8)
        score = self.relu(self.deconv2(score))

        x3 = self.score_pool3(x3)
        # element-wise add, size=(N, 256, x.H/8, x.W/8)
        score = self.bn2(score + x3)
        # size=(N, num_classes, x.H/4, x.W/4)
        score = self.bn3(self.relu(self.deconv3(score)))
        # size=(N, num_classes, x.H/2, x.W/2)
        score = self.bn4(self.relu(self.deconv4(score)))
        # size=(N, 32, x.H, x.W)
        score = self.bn5(self.relu(self.deconv5(score)))

        return score  # size=(N, self.n_class, x.H/1, x.W/1)
