import torch
import torch.nn as nn

from core.config import config as cfg

from models.spatial_softargmax import SoftArgMax


def conv3x3(in_planes, out_planes):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3,
                     padding=1, bias=False)


class ResDeconv(nn.Module):

    def __init__(self):
        self.config = cfg
        self.inplanes = 64
        self.deconv_with_bias = False

        super(ResDeconv, self).__init__()
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, padding=3,
                               bias=False)
        self.bn1 = nn.BatchNorm2d(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        layers = self.config['network']['layers']
        self.block1 = self.make_block(SubNet, 64, layers[0])
        self.block2 = self.make_block(SubNet, 128, layers[1])
        self.block3 = self.make_block(SubNet, 256, layers[2])
        self.block4 = self.make_block(SubNet, 512, layers[3])

        # used for deconv layers
        self.deconv_block = self._make_deconv_layer(
            self.config['network']['deconv_layers'],
            self.config['network']['deconv_filter'],
            self.config['network']['deconv_kernel']
        )

        num_joints = self.config['dataset']['current']['num_joints']
        self.final_layer = nn.Conv2d(
            in_channels=256,
            out_channels=num_joints,
            kernel_size=1
        )

        img_size = self.config['network']['heatmap_size']
        self.softargmax = SoftArgMax(
            width=img_size[0],
            height=img_size[1],
            channel=num_joints
        )

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.orthogonal_(
                    m.weight)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def make_block(self, block, planes, blocks):
        downsample = None
        if self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, downsample))
        layers.append(nn.MaxPool2d(kernel_size=3, stride=2, padding=1))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)

    def deconv_cfg(self, deconv_kernel, index):
        if deconv_kernel == 4:
            padding = 1
            output_padding = 0
        elif deconv_kernel == 3:
            padding = 1
            output_padding = 1
        elif deconv_kernel == 2:
            padding = 0
            output_padding = 0

        return deconv_kernel, padding, output_padding

    def _make_deconv_layer(self, num_layers, num_filters, num_kernels):
        layers = []
        for i in range(num_layers):
            kernel, padding, output_padding = \
                self.deconv_cfg(num_kernels[i], i)

            planes = num_filters[i]
            layers.append(
                nn.ConvTranspose2d(
                    in_channels=self.inplanes,
                    out_channels=planes,
                    kernel_size=kernel,
                    stride=2,
                    padding=padding,
                    output_padding=output_padding,
                    bias=self.deconv_with_bias))
            layers.append(nn.BatchNorm2d(planes))
            layers.append(nn.ReLU(inplace=True))
            self.inplanes = planes

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.block4(x)

        x = self.deconv_block(x)
        x = self.final_layer(x)

        x = self.softargmax(x)

        return x


class SubNet(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, downsample=None):
        super(SubNet, self).__init__()
        self.conv1 = conv3x3(inplanes, planes)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out
