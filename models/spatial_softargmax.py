import torch
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import numpy as np
from core.config import config


class SoftArgMax(torch.nn.Module):
    def __init__(self, channel=None):
        super(SoftArgMax, self).__init__()
        self.height = config['network']['heatmap_size'][0]
        self.width = config['network']['heatmap_size'][1]
        self.channel = config['dataset']['current']['num_joints'] if channel is None else channel

        pos_x, pos_y = np.meshgrid(
            np.linspace(0., 1., self.height),
            np.linspace(0., 1., self.width)
        )
        pos_x = torch.from_numpy(pos_x.reshape(self.height*self.width)).float()
        pos_y = torch.from_numpy(pos_y.reshape(self.height*self.width)).float()
        self.register_buffer('pos_x', pos_x)
        self.register_buffer('pos_y', pos_y)

    def forward(self, feature):
        feature = feature.view(-1, self.height*self.width)

        softmax_attention = F.softmax(feature, dim=-1)
        expected_x = torch.sum(
            self.pos_x*softmax_attention, dim=1, keepdim=True)
        expected_y = torch.sum(
            self.pos_y*softmax_attention, dim=1, keepdim=True)
        expected_xy = torch.cat(
            [expected_y * self.height, expected_x * self.width], dim=1)
        feature_keypoints = expected_xy.view(-1, self.channel, 2)

        return feature_keypoints
