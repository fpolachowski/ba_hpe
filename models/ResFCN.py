import torch
import torch.nn as nn
import numpy as np

from core.config import config

from models.spatial_softargmax import SoftArgMax
from models.FCN8s import FCN8s
from models.FCN8s_v2 import FCN8s_v2


def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, bias=False)


class ResFCN(nn.Module):
    def __init__(self, num_joints=None):
        self.inplanes = 64
        super(ResFCN, self).__init__()
        self.conv1 = nn.Conv2d(
            in_channels=3, out_channels=self.inplanes, kernel_size=7, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(num_features=self.inplanes)
        self.activation = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        layers = config['network']['layers']
        self.block1 = self.make_block(SubNet, 128, layers[0])
        self.block2 = self.make_block(SubNet, 256, layers[1])
        self.block3 = self.make_block(SubNet, 512, layers[2])
        self.block4 = self.make_block(SubNet, 512, layers[3])

        self.fcn = FCN8s_v2(num_joints)
        self.softargmax = SoftArgMax(num_joints)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.orthogonal_(
                    m.weight)
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, x):
        output = {}
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activation(x)
        x = self.maxpool(x)
        x = self.block1(x)
        x = self.block2(x)
        output['x3'] = x
        x = self.block3(x)
        output['x4'] = x
        x = self.block4(x)
        output['x5'] = x
        heat_map = x

        x = self.fcn(output)

        x = self.softargmax(x)

        return x, heat_map

    def make_block(self, block, planes, blocks):
        downsample = None
        if self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=1, bias=False),
                nn.BatchNorm2d(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, downsample))
        layers.append(nn.MaxPool2d(kernel_size=3, stride=2, padding=1))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes))

        return nn.Sequential(*layers)


class SubNet(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, downsample=None):
        super(SubNet, self).__init__()
        self.conv1 = conv3x3(inplanes, planes)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = nn.BatchNorm2d(planes)
        self.downsample = downsample

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out += residual
        out = self.relu(out)

        return out
