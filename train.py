import torch
import numpy
from core.config import config
from core.functions import evaluate, train_epoch
from models.ResDeconv import ResDeconv
from models.ResFCN import ResFCN
from core.loss import ElasticNetLossPoses, DistanceLossPoses
from util.builder import Builder
from util.explorer import Logger
from util.utils import get_optimizer, get_dataset, seed_everything, createSplit


dataset = get_dataset()


for run in Builder.create(config['parameters']):
    for i in range(config['train']['num_runs']):
        seed_everything()
        log = Logger(run, i)

        network = ResFCN()
        network.to(config['device'])
        optimizer = get_optimizer(run, network)
        criterion = DistanceLossPoses()

        log.next_Kfold()

        train_loader, eval_loader = createSplit(
            dataset, run.batch_size, shuffle=True, split=0.1)

        for epoch in range(run.end_epoch):
            train_epoch(train_loader, network, criterion,
                        optimizer, epoch, log)

            evaluate(eval_loader, network, criterion, log, epoch)

        log.endfold(network, len(eval_loader))

        if config['store_results']:
            log.storeNetwork(network)
