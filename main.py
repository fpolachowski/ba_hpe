import torch
import numpy
from sklearn.model_selection import KFold
from core.config import config
from core.functions import evaluate, train_epoch
from models.ResDeconv import ResDeconv
from core.loss import DistanceLossPoses
from util.builder import Builder
from util.explorer import Logger
from util.utils import get_optimizer, get_dataset, seed_everything, createLoader


dataset = get_dataset()

kf = KFold(n_splits=config['train']['kf_split'], shuffle=config['train']
           ['shuffle'], random_state=config['train']['kf_rs'])

for run in Builder.create(config['parameters']):
    for i in range(config['train']['num_runs']):
        seed_everything()
        log = Logger(run, i)
        for train, test in kf.split(dataset):
            network = ResDeconv()
            network.to(config['device'])
            optimizer = get_optimizer(run, network)
            criterion = DistanceLossPoses()

            log.next_Kfold()

            train_loader, eval_loader = createLoader(
                dataset, run.batch_size, train, test)

            for epoch in range(run.end_epoch):
                train_epoch(train_loader, network, criterion,
                            optimizer, epoch, log)

                evaluate(eval_loader, network, criterion, log, epoch)

            log.endfold(network, len(eval_loader))
