import torch
import numpy as np
import sys
import json
from glob import glob
from core.config import config
from models.ResFCN import ResFCN
from util.utils import get_dataset, seed_everything, createSplit, getMeanDist
from util.metrics import dist, pcp, pcpm, pckh, pckb
from util.visualizer import showImage_AnnCoords, save


def flic_to_mpii(predictions):
    res = torch.zeros(predictions.size(0), 16, 2, device=predictions.device)
    order = [12, 11, 10, 13, 14, 15, 2, 3, 8, 9, 7]
    for i in range(predictions.size(0)):
        for j in range(11):
            res[i, order[j]] = predictions[i, j]
    return res


def mpii_to_flic(predictions):
    res = torch.zeros(predictions.size(0), 11, 2, device=predictions.device)
    order = [-1, -1, 6, 7, -1, -1, -1, 10, 8, 9, 2, 1, 0, 3, 4, 5]
    for i in range(predictions.size(0)):
        for j in range(16):
            if order[j] != -1:
                res[i, order[j]] = predictions[i, j]
    return res


dataset = get_dataset()
files = glob('store/*.pth')

print(files)

for index, path in enumerate(files):
    print('###### Run No. {} ############################'.format(index))
    seed_everything()

    num_joints = None
    if config['dataset']['current']['name'] == 'FLIC':
        num_joints = 11
    else:
        num_joints = 16

    network = ResFCN(num_joints=num_joints)
    network.load_state_dict(torch.load(path, map_location='cuda:0'))
    network.eval()
    network.to(config['device'])

    train_loader, eval_loader = createSplit(
        dataset, config['eval']['batch_size'], shuffle=True, split=0.1)

    fileName = config['dataset']['current']['name'] + \
        '_' + path.split('\\')[-1].replace('.pth', '.json')

    alphas = [0.05]
    np.concatenate((
        np.linspace(0, 0.1, num=101), np.linspace(0.2, 0.5, num=11)))

    # meandist = getMeanDist(train_loader, eval_loader)
    # print(meandist)

    runs = {}
    for alpha in alphas:
        runs[alpha] = {}
        for name in config['dataset']['current']['joints']:
            runs[alpha][name] = 0.

    for index, batch in enumerate(eval_loader):
        images, labels = batch[0].to(
            config['device']), batch[1].to(config['device'])
        predictions, heat_maps = network(images)

        # showImage_AnnCoords(heat_maps, predictions, labels)

        for alpha in alphas:
            accuracy = pckb(predictions, labels, torso=(3, 12), alpha=alpha)
            # accuracy = pcpm(predictions, labels, meandist, alpha=alpha)
            # accuracy = pcp(predictions, labels, alpha=alpha)
            # accuracy = pckh(predictions, labels, alpha=alpha)
            for name, acc in zip(config['dataset']['current']['joints'], accuracy):
                runs[alpha][name] += acc

        sys.stdout.write(
            '\r{}/{} done'.format(index + 1, len(eval_loader)))
        sys.stdout.flush()

    for alpha in runs:
        for key in runs[alpha]:
            runs[alpha][key] /= len(eval_loader)

    if config['dataset']['current']['name'] == 'MPII':
        for alpha in runs:
            print('\n')
            print((runs[alpha]['upper neck'] +
                   runs[alpha]['head top']) / 0.02)
            print((runs[alpha]['left shoulder'] +
                   runs[alpha]['right shoulder']) / 0.02)
            print((runs[alpha]['right elbow'] +
                   runs[alpha]['left elbow']) / 0.02)
            print((runs[alpha]['right wrist'] +
                   runs[alpha]['left wrist']) / 0.02)
            print((runs[alpha]['right hip'] + runs[alpha]
                   ['left hip']) / 0.02)
            print((runs[alpha]['right knee'] +
                   runs[alpha]['left knee']) / 0.02)
            print((runs[alpha]['right ankle'] +
                   runs[alpha]['left ankle']) / 0.02)
        av = 0.
        for key in runs[alpha]:
            if key not in ['pelvis', 'thorax']:
                av += runs[alpha][key] / 14
        print(av * 100)
    else:
        for alpha in runs:
            print('\n')
            print((runs[alpha]['right eye'] +
                   runs[alpha]['left eye'] + runs[alpha]['nose']) / 0.03)
            print((runs[alpha]['right shoulder'] +
                   runs[alpha]['left shoulder']) / 0.02)
            print((runs[alpha]['right elbow'] +
                   runs[alpha]['left elbow']) / 0.02)
            print((runs[alpha]['right wrist'] +
                   runs[alpha]['left wrist']) / 0.02)
            print((runs[alpha]['right hip'] + runs[alpha]
                   ['left hip']) / 0.02)
            av = 0.
            for key in runs[alpha]:
                av += runs[alpha][key] / 11
            print(av * 100)

    with open('runs/' + fileName, 'w') as f:
        json.dump(runs, f)

    print('###### End next run ############################')
