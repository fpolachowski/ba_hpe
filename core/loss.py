import torch
import torch.nn as nn
import numpy as np

"""
    Loss Function
"""


class ElasticNetLossPoses(nn.Module):
    def __init__(self):
        super(ElasticNetLossPoses, self).__init__()
        self.mae = nn.L1Loss()
        self.mse = nn.MSELoss()

    def forward(self, preds, targets):
        loss = 0.
        for pred, label in zip(preds, targets):
            l1 = self.mae(pred, label)
            l2 = self.mse(pred, label)
            loss += l1 + l2
        return loss


class DistanceLossPoses(nn.Module):
    def __init__(self):
        super(DistanceLossPoses, self).__init__()
        self.dist = torch.norm

    def forward(self, preds, targets):
        loss = 0.
        for pred, label in zip(preds, targets):
            l1 = self.dist(pred - label)
            l2 = l1 ** 2
            loss += (l1 + l2) / len(pred)
        return loss
