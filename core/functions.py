import sys
from util.metrics import pcp
from core.config import config


def train_epoch(loader, network, criterion, optimizer, epoch, logger):
    network.train()
    optimizer.zero_grad()
    for batch in loader:
        images, labels = batch[0].to(
            config['device']), batch[1].to(config['device'])
        preds = network(images)
        loss = criterion(preds, labels) * \
            (1 / config['train']['accumulation_steps'])
        loss.backward()
        if epoch % config['train']['accumulation_steps'] == 0:
            optimizer.step()
            optimizer.zero_grad()

        logger.step(epoch, loss, preds, labels)


def evaluate(loader, network, criterion, logger, epoch):
    network.eval()
    for index, batch in enumerate(loader):
        images, labels = batch[0].to(
            config['device']), batch[1].to(config['device'])
        preds = network(images)
        loss = criterion(preds, labels)
        logger.evaluate(preds, labels, loss)
        if index == 0:
            logger.saveImage(images, preds, labels, epoch)

    logger.endEval(len(loader))
