from torch.utils.data.sampler import Sampler


class SequentialSampler(Sampler):
    r"""Samples elements of a given subset sequentially, always in the same order.

    Arguments:
        data_source (Dataset): dataset to sample from
    """

    def __init__(self, subset):
        self.subset = subset

    def __iter__(self):
        return iter(self.subset)

    def __len__(self):
        return len(self.subset)
