debug = True
cross = True

config = dict()
config['debug'] = debug
config['cross'] = cross
config['device'] = 'cuda:1' if not debug else 'cuda:0'
config['doc_folder'] = 'runs'
config['model_folder'] = 'store'
config['image_folder'] = 'images'
config['doc_steps'] = 4
config['pcp_acc'] = 0.5
config['num_images'] = 6
config['store_results'] = True


mpii = dict()
mpii['name'] = 'MPII'
mpii['root'] = '/data/fpolacho/MPII' if not debug else '../MPII'
mpii['num_joints'] = 16
mpii['torsobox_joints'] = [3, 12]
mpii['mean'] = [0.4749, 0.4668, 0.4275]
mpii['std'] = [0.2242, 0.2224, 0.2210]
mpii['joints'] = ['right ankle', 'right knee', 'right hip', 'left hip', 'left knee', 'left ankle', 'pelvis', 'thorax',
                  'upper neck', 'head top', 'right wrist', 'right elbow', 'right shoulder', 'left shoulder', 'left elbow', 'left wrist']
mpii['body'] = [(0, 1), (1, 2), (2, 3), (3, 2), (4, 3), (5, 4), (6, 2), (7, 8),
                (8, 9), (9, 8), (10, 11), (11, 12), (12, 13), (13, 12), (14, 13), (15, 14)]
mpii['body_vis'] = [[0, 1], [1, 2], [2, 6], [2, 12], [3, 13], [3, 6],  [4, 3], [5, 4], [
    7, 8], [7, 13], [7, 12], [8, 13], [8, 12], [8, 9], [10, 11], [11, 12], [13, 14], [14, 15]]


flic = dict()
flic['name'] = 'FLIC'
flic['root'] = '/data/fpolacho/FLIC' if not debug else '../FLIC'
flic['num_joints'] = 11
flic['torsobox_joints'] = [7, 0]
flic['mean'] = [0.2523, 0.2246, 0.2098]
flic['std'] = [0.2242, 0.2224, 0.2210]
flic['joints'] = ['right shoulder', 'right elbow', 'right wrist', 'left shoulder',
                  'left elbow', 'left wrist', 'right hip', 'left hip', 'right eye', 'left eye', 'nose']
flic['body'] = [(2, 1), (1, 0), (0, 10), (6, 7), (5, 4),
                (4, 3), (7, 6), (3, 10), (10, 8), (9, 10), (8, 10)]
flic['body_vis'] = [[9, 10], [8, 10], [10, 0], [3, 0], [3, 10], [
    4, 3], [7, 3], [5, 4], [0, 6], [6, 0], [6, 7], [1, 0], [2, 1]]

dataset = dict()
config['dataset'] = dataset
dataset['current'] = mpii
dataset['MPII'] = mpii
dataset['FLIC'] = flic
dataset['image_size'] = [256, 256]
dataset['crop_offset'] = 30

net = dict()
config['network'] = net
net['layers'] = [3, 4, 6, 3]
net['deconv_layers'] = 3
net['deconv_filter'] = [256, 256, 256]
net['deconv_kernel'] = [4, 4, 4]
net['heatmap_size'] = [256, 256]


train = dict()
config['train'] = train
train['num_runs'] = 1
train['optim'] = 'adam'
train['lr'] = [0.001]
train['momentum'] = [0.9]
train['weight_decay'] = [0.001]
train['end_epoch'] = [150]
train['batch_size'] = [20]
train['accumulation_steps'] = 1
train['shuffle'] = True
train['kf_split'] = 10
train['kf_rs'] = 42


evaluate = dict()
config['eval'] = evaluate
evaluate['batch_size'] = 10
evaluate['num_runs'] = 1
evaluate['alphas'] = 20


parameters = dict(
    lr=train['lr'],
    momentum=train['momentum'] if config['train']['optim'] == 'sgd' else [0],
    weight_decay=train['weight_decay'],
    batch_size=train['batch_size'],
    end_epoch=train['end_epoch']
)
config['parameters'] = parameters
